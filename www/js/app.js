(function(){
	
	

	
	var app = angular.module('productSelector',['filters']);


	app.controller('ProductController',function(){
		this.products = productDatas;
		this.product0 = this.products[0];
		this.product1 = this.products[1];
		this.product2 = this.products[2];
		this.tab = 0;
		
		this.selectTab = function (value){
			this.tab = value;
		}
		
		this.isTabSelected = function(value){			
			return this.tab === value;
		}
		
		this.isMobile = function(){		
			return mobilecheck();
		}
		
		
	});	
	
	
	
	//CUSTOM DIRECTIVES FOR RESPONSIVE HANDLING
	app.directive('responsiveMobile',function(){
		var directive = {};

		directive.restrict = 'E'; 
		directive.template="";

		if(mobilecheck()){
			//IF MOBILE WE ALLOW IT
            directive.transclude = false;
        }else {
        	//OTHERWISE WE REPLACE IT
            
            directive.transclude = true;
        } 
				
		return directive;
		
		
	});
	
	app.directive('responsiveDesktop',function(){
		var directive = {};

		directive.restrict = 'E'; 
		directive.template="";
		if(mobilecheck()){
			//IF MOBILE WE ALLOW IT
            directive.transclude = true;
        }else {
        	//DESKTOP == ALLOW
            directive.transclude = false;
        } 
				
		return directive;
		
		
	});
		
	
})();


//DATAS TO LATER BRING FROM WEB SERVICE
var productDatas = [{
		
		name: "Acer Aspire E1-572",
		price: "399.00",
		os: "Windows 8.1",
		dimensions: "380 x 27 x 255",
		weight: "2.193",
		transportWeight: "2.47",
		processor: "Intel Core i5-4200U",
		processorSpeed: "1.6",
		ram: "4",
		typeOfStorage: "Spinning HD 5400",
		storageCapacity: "500"
	},{
		
		name: "Acer Aspire S3-392G",
		price: "899.99",
		os: "Windows 8.1",
		dimensions: "221 x 17 x 124",
		weight: "1.618",
		transportWeight: "1.97",
		processor: "Intel Core i5-4200U",
		processorSpeed: "1.6",
		ram: "4",
		typeOfStorage: "Spinning HD 5400",
		storageCapacity: "500"
	},{
		
		name: "Acer Aspire V5-573",
		price: "599.95",
		os: "Windows 8.1",
		dimensions: "380 x 27 x 255",
		weight: "1.618",
		transportWeight: "2.47",
		processor: "Intel Core i5-4200U",
		processorSpeed: "1.6",
		ram: "4",
		typeOfStorage: "Spinning HD 5400",
		storageCapacity: "1000"
	},{
		
		name: "Acer V5-552",
		price: "479.99",
		os: "Windows 8.1",
		dimensions: "377 x 23 x 255",
		weight: "2.045",
		transportWeight: "1.97",
		processor: "Intel Core i7-4500U",
		processorSpeed: "1.8",
		ram: "8",
		typeOfStorage: "Spinning HD 5400",
		storageCapacity: "1000"
	},{
		
		name: "Acer Aspire E1-572",
		price: "585.00",
		os: "Windows 8.0",
		dimensions: "378 x 24 x 254",
		weight: "2",
		transportWeight: "2.46",
		processor: "AMD A10-5757M",
		processorSpeed: "2.1",
		ram: "6",
		typeOfStorage: "Spinning HD 5400",
		storageCapacity: "500"
	},{
		
		name: "Apple MacBook Air 11-inch (2013)",
		price: "669.99",
		os: "OSX 10.8.4",
		dimensions: "299 x 13.4 x 192",
		weight: "1.079",
		transportWeight: "2.41",
		processor: "Intel Core i5-4250U",
		processorSpeed: "1.3",
		ram: "4",
		typeOfStorage: "SSD",
		storageCapacity: "128"
	},{
		
		name: "Apple MacBook Air 13-inch (2013)",
		price: "856.39",
		os: "OSX 10.8.4",
		dimensions: "325 x 12.6 x 227",
		weight: "1.328",
		transportWeight: "1.39",
		processor: "Intel Core i5-4250U",
		processorSpeed: "1.3",
		ram: "4",
		typeOfStorage: "SSD",
		storageCapacity: "128"
	},{
		
		name: "Apple Macbook Pro Retina 13-inch (2013)",
		price: "1338.00",
		os: "Mac OS 10.9",
		dimensions: "314 x 17.8 x 219",
		weight: "1.566",
		transportWeight: "1.94",
		processor: "Intel Core i5-4258U",
		processorSpeed: "2.4",
		ram: "4",
		typeOfStorage: "SSD",
		storageCapacity: "128"
	},{
		
		name: "Apple Macbook Pro Retina 15-inch (2013)",
		price: "1338.00",
		os: "Mac OS 10.9",
		dimensions: "359 x 18 x 247",
		weight: "1.997",
		transportWeight: "2.37",
		processor: "Intel Core i7-4750HQ",
		processorSpeed: "2",
		ram: "8",
		typeOfStorage: "SSD",
		storageCapacity: "256"
	}];

//CUSTOM FILTERS
angular.module('filters', []).
    filter('truncate', function () {
        return function (text, length, end) {
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length || text.length - end.length <= length) {
                return text;
            }
            else {
                return String(text).substring(0, length-end.length) + end;
            }

        };
    });


//MOBILE CHECKING
var mobilecheck = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check; 
}

